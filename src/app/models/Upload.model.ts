export class Upload {

  // $key: string;
  name: string;
  url: string;
  progress: number;
  createdAt: Date = new Date();

  constructor(public file: File) {
    this.file = file;
  }
}

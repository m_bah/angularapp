import { Component } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {
    const config = {
      apiKey: 'AIzaSyC9TMYNLuuNR1mVBFuEMTlf1GaTphGRUjY',
      authDomain: 'angular-9f934.firebaseapp.com',
      databaseURL: 'https://angular-9f934.firebaseio.com',
      projectId: 'angular-9f934',
      storageBucket: 'angular-9f934.appspot.com',
      messagingSenderId: '499029804598'
    };
    firebase.initializeApp(config);
  }
}

import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private route: ActivatedRoute) { }

  createNewUser(email: string, password: string) {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email, password)
          .then(
            () => {
              resolve();
            },
            (error) => {
              reject(error);
            }
          );
      }
    );
  }

  signInUser(email: string, password: string) {
    return new  Promise(
      ((resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
          .then(
            () => {
              resolve();
            },
            (err) => {
              reject(err);
            }
          );
      })
    );
  }

  signOut() {
    firebase.auth().signOut();
  }
}

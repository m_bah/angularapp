import { Injectable } from '@angular/core';
import {Book} from '../models/Book.model';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';
import {Upload} from '../models/Upload.model';
import {toASCII} from 'punycode';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books: Book[] = [];
  booksSubject =  new Subject<Book[]>();
  constructor() { }

  emitBooks() {
    this.booksSubject.next(this.books);
  }

  saveBooks() {
    firebase.database().ref('/books').set(this.books);
  }

  getBooks() {
    firebase.database().ref('/books')
      .on('value', (data) => {
        this.books = data.val() ? data.val() : [];
        this.emitBooks();
      });
  }

  getSingleBook(id: number) {
   return new Promise(
     ((resolve, reject) => {
       firebase.database().ref('/books/' + id).once('value')
         .then(
           (data) => {
             resolve(data.val());
           },
           (err) => {
             reject(err);
           }
         );

     })
   );
  }

  createNewBook(book: Book) {
    this.books.push(book);
    this.saveBooks();
    this.emitBooks();
  }

  removeBook(book: Book) {
    if (book.photo) {
      const storageRef = firebase.storage().refFromURL(book.photo);
      storageRef.delete().then(
        () => {
            console.log('photo supprimée !');
        },
        (err) => {
          console.log('Fichier introuvable : ' + err);
        }
      );
    }
    const bookIndex = this.books.findIndex(
      (bookEl) => {
        if (bookEl === book) {
          return true;
        }
      }
    );
    this.books.splice(bookIndex, 1);
    this.saveBooks();
    this.emitBooks();
  }

  uploadFile(upload: Upload) {
    return new Promise(
      (resolve, reject) => {
        const storageRef = firebase.storage().ref();
        const uploadTask = storageRef.child('images/' + upload.file.name).put(upload.file);
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
          (snapshot: firebase.storage.UploadTaskSnapshot) => {
                              // upload in progress
            upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            // console.log('Upload is ' + this.progress + '% done');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                // console.log('Upload is running');
                break;
            }
          },
          (error) => {
            console.log('Erreur de chargement ! : ' + error);
            reject();
          },
          () => {
            resolve(uploadTask.snapshot.ref.getDownloadURL());
          }
        );
      }
    );
  }
}

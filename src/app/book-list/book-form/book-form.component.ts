import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {BooksService} from '../../services/books.service';
import {Book} from '../../models/Book.model';
import {Upload} from '../../models/Upload.model';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  bookForm: FormGroup;
  fileIsUploading = false;
  fileUrl: string;
  fileUploaded = false;
  currentUpload: Upload;
  selectedFiles: FileList;

  constructor(private formBuilder: FormBuilder, private booksService: BooksService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      synopsie: ''
    });
  }

  onSaveBook() {
    const title = this.bookForm.get('title').value;
    const auteur = this.bookForm.get('author').value;
    const synopsie = this.bookForm.get('synopsie').value;
    const newBook = new Book(title, auteur);
    newBook.synopsie = synopsie;
    if (this.fileUrl && this.fileUrl !== '') {
      newBook.photo = this.fileUrl;
    }
    this.booksService.createNewBook(newBook);
    this.router.navigate(['/books']);
  }

  onUploadFile(event) {
    this.fileIsUploading = true;
    this.selectedFiles = event.target.files;
    const file = this.selectedFiles.item(0);
    this.currentUpload = new Upload(file);
    this.booksService.uploadFile(this.currentUpload)
      .then(
        (url: string) => {
          this.fileUrl = url;
          this.fileIsUploading = false;
          this.fileUploaded = true;
          // console.log(this.fileUrl);
        }
      );
  }


}

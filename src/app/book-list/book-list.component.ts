import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Book} from '../models/Book.model';
import {Subscription} from 'rxjs';
import {BooksService} from '../services/books.service';
import {Router} from '@angular/router';
import {MatDialog, MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {

  books: Book[];
  index: number;
  booksSubscription: Subscription;
  @ViewChild('popUp') popUp: TemplateRef<'#popUp'>;

  constructor(private booksService: BooksService,
              private router: Router,
              private dialog: MatDialog,
              private  snackBar: MatSnackBar) { }

  ngOnInit() {
    this.booksSubscription = this.booksService.booksSubject.subscribe(
      (books: Book[]) => {
        this.books = books;
      }
    );
    this.booksService.getBooks();
    this.booksService.emitBooks();
  }

  onNewBook() {
    this.router.navigate(['/books', 'new']);
  }

  onDeleteBook(book: Book) {
    this.booksService.removeBook(book);
  }

  onViewBook(id: number) {
    this.router.navigate(['/books', 'view', id]);
  }

  ngOnDestroy () {
    this.booksSubscription.unsubscribe();
  }

  openDialog(event) {
    const dialogRef = this.dialog.open(this.popUp, {
      data: event.target
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.router.navigate(['/books']);
      if (result) {
        this.openSnackBar();
      }
    });
  }

  openSnackBar() {
    this.snackBar.open('Livre supprimé !', 'deleted',{
      duration: 3000,
      verticalPosition: 'top'
    });
  }


}
